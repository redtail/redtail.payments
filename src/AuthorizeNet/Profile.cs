using System;
using System.Collections.Generic;

namespace RedTail.Payments.AuthorizeNet
{
  public class Profile
  {
    public String MerchantCustomerId { get; set; }
    public String Description { get; set; }
    public String Email { get; set; }
    public String CustomerProfileId { get; set; }
    public List<PaymentProfile> PaymentProfiles { get; set; }
  }
}
