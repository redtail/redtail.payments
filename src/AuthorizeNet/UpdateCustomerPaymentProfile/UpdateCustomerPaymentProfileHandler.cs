using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet.UpdateCustomerPaymentProfile
{
  public class UpdateCustomerPaymentProfileHandler
    : AuthorizeNetHandler<UpdateCustomerPaymentProfileRequest, UpdateCustomerPaymentProfileResponse>
  {
    public UpdateCustomerPaymentProfileHandler(UpdateCustomerPaymentProfileRequest request, Boolean stopOnError)
      : base(request, stopOnError)
    {

    }

    private String Json = null;
    private String ValidationDirectResponse = null;

    protected override void Command(UpdateCustomerPaymentProfileRequest request)
    {
      this.Json = @"
        {
          ""updateCustomerPaymentProfileRequest"": " + JsonConvert.SerializeObject(request, Formatting.None, Properties.JsonSerializerSettings) + @"
        }
      ";

      String result = this.Send(this.Json);
      UpdateCustomerPaymentProfileResponse response = JsonConvert.DeserializeObject<UpdateCustomerPaymentProfileResponse>(result);

      this.ValidationDirectResponse = response.ValidationDirectResponse;
    }

    protected override void Done(UpdateCustomerPaymentProfileResponse response)
    {
      base.Done(response);
      response.ValidationDirectResponse = this.ValidationDirectResponse;
    }

    protected override void Error(UpdateCustomerPaymentProfileResponse response)
    {
      base.Error(response);
    }
  }
}
