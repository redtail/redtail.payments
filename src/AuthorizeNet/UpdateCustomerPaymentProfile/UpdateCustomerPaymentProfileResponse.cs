using System;

namespace RedTail.Payments.AuthorizeNet.UpdateCustomerPaymentProfile
{
  public class UpdateCustomerPaymentProfileResponse
    : AuthorizeNetResponse
  {
    public String ValidationDirectResponse { get; set; }
  }
}
