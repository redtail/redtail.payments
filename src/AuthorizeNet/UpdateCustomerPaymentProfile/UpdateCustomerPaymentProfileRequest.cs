using System;

namespace RedTail.Payments.AuthorizeNet.UpdateCustomerPaymentProfile
{
  public class UpdateCustomerPaymentProfileRequest
    : AuthorizeNetRequest
  {
    public MerchantAuthentication MerchantAuthentication => Properties.MerchantAuthentication;
    public String CustomerProfileId { get; set; }
    public PaymentProfile PaymentProfile { get; set; }
    public String ValidationMode { get; set; }
  }
}
