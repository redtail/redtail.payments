using System;

namespace RedTail.Payments.AuthorizeNet
{
  public class Message
  {
    public String Code { get; set; }
    public String Text { get; set; }
  }
}
