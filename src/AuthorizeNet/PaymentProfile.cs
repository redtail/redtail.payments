using System;

namespace RedTail.Payments.AuthorizeNet
{
  public class PaymentProfile
  {
    public String CustomerType { get; set; }
    public CustomerAddress BillTo { get; set; }
    public Payment Payment { get; set; }
    public Boolean? DefaultPaymentProfile { get; set; }
    public String CustomerPaymentProfileId { get; set; }
  }
}
