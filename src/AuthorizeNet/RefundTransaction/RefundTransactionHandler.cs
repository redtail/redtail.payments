using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet.RefundTransaction
{
  public class RefundTransactionHandler
    : AuthorizeNetHandler<RefundTransactionRequest, RefundTransactionResponse>
  {
    public RefundTransactionHandler(RefundTransactionRequest request, Boolean stopOnError)
      : base(request, stopOnError)
    {

    }

    private TransactionResponse TransactionResponse = null;
    private String Json = null;
    private String RefId = null;

    protected override void Command(RefundTransactionRequest request)
    {
      this.Json = @"
        {
          ""createTransactionRequest"": " + JsonConvert.SerializeObject(request, Formatting.None, Properties.JsonSerializerSettings) + @"
        }
      ";

      String result = this.Send(this.Json);
      RefundTransactionResponse response = JsonConvert.DeserializeObject<RefundTransactionResponse>(result);

      this.TransactionResponse = response.TransactionResponse;
      this.RefId = response.RefId;
    }

    protected override void Done(RefundTransactionResponse response)
    {
      base.Done(response);
      response.TransactionResponse = this.TransactionResponse;
      response.RefId = this.RefId;
    }

    protected override void Error(RefundTransactionResponse response)
    {
      base.Error(response);
    }
  }
}
