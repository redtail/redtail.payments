using System;

namespace RedTail.Payments.AuthorizeNet.RefundTransaction
{
  public class RefundTransactionResponse
    : AuthorizeNetResponse
  {
    public TransactionResponse TransactionResponse { get; set; }
    public String RefId { get; set; }
  }
}
