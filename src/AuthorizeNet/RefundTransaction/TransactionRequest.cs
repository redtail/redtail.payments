using System;

namespace RedTail.Payments.AuthorizeNet.RefundTransaction
{
  public class TransactionRequest
  {
    public String TransactionType { get; } = "refundTransaction";
    public String Amount { get; set; }
    public Payment Payment { get; set; }
    public String RefTransId { get; set; }
  }
}
