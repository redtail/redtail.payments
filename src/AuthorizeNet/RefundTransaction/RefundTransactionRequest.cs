using System;

namespace RedTail.Payments.AuthorizeNet.RefundTransaction
{
  public class RefundTransactionRequest
    : AuthorizeNetRequest
  {
    public MerchantAuthentication MerchantAuthentication => Properties.MerchantAuthentication;
    public String RefId { get; set; }
    public TransactionRequest TransactionRequest { get; set; }
  }
}
