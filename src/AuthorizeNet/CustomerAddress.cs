using System;

namespace RedTail.Payments.AuthorizeNet
{
  public class CustomerAddress
  {
    public String FirstName { get; set; }
    public String LastName { get; set; }
    public String Address { get; set; }
    public String City { get; set; }
    public String State { get; set; }
    public String Zip { get; set; }
    public String Country { get; set; }
    public String PhoneNumber { get; set; }
  }
}
