using System;

namespace RedTail.Payments.AuthorizeNet
{
  public class TransactionRequest
  {
    public String TransactionType { get; set; }
    public String RefTransId { get; set; }
  }
}
