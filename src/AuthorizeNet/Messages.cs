using System;
using System.Collections.Generic;

namespace RedTail.Payments.AuthorizeNet
{
  public class Messages
  {
    public List<Message> Message { get; set; }
    public String ResultCode { get; set; }

    public Boolean IsError => this.ResultCode == "Error";
  }
}
