using System;
using System.Collections.Generic;

namespace RedTail.Payments.AuthorizeNet
{
  public class Error
  {
    public String ErrorCode { get; set; }
    public String ErrorText { get; set; }
  }
}
