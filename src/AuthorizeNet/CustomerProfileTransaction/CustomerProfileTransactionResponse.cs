using System;

namespace RedTail.Payments.AuthorizeNet.CustomerProfileTransaction
{
  public class CustomerProfileTransactionResponse
    : AuthorizeNetResponse
  {
    public TransactionResponse TransactionResponse { get; set; }
    public String RefId { get; set; }
  }
}
