using System;

namespace RedTail.Payments.AuthorizeNet.CustomerProfileTransaction
{
  public class Profile
  {
    public String CustomerProfileId { get; set; }
    public PaymentProfile PaymentProfile { get; set; }
  }
}
