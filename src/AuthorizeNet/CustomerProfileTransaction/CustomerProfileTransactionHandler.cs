using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet.CustomerProfileTransaction
{
  public class CustomerProfileTransactionHandler
    : AuthorizeNetHandler<CustomerProfileTransactionRequest, CustomerProfileTransactionResponse>
  {
    public CustomerProfileTransactionHandler(CustomerProfileTransactionRequest request, Boolean stopOnError)
      : base(request, stopOnError)
    {

    }

    private TransactionResponse TransactionResponse = null;
    private String Json = null;
    private String RefId = null;

    protected override void Command(CustomerProfileTransactionRequest request)
    {
      this.Json = @"
        {
          ""createTransactionRequest"": " + JsonConvert.SerializeObject(request, Formatting.None, Properties.JsonSerializerSettings) + @"
        }
      ";

      String result = this.Send(this.Json);
      CustomerProfileTransactionResponse response = JsonConvert.DeserializeObject<CustomerProfileTransactionResponse>(result);

      this.TransactionResponse = response.TransactionResponse;
      this.RefId = response.RefId;
    }

    protected override void Done(CustomerProfileTransactionResponse response)
    {
      base.Done(response);
      response.TransactionResponse = this.TransactionResponse;
      response.RefId = this.RefId;
    }

    protected override void Error(CustomerProfileTransactionResponse response)
    {
      base.Error(response);
    }
  }
}
