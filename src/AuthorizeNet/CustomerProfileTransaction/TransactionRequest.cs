using System;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet.CustomerProfileTransaction
{
  public class TransactionRequest
  {
    public String TransactionType => this.Type.GetName();
    public String Amount { get; set; }
    public Profile Profile { get; set; }

    [JsonIgnore]
    public TransactionType Type { get; set; }
  }
}
