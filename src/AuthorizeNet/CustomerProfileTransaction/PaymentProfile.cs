using System;

namespace RedTail.Payments.AuthorizeNet.CustomerProfileTransaction
{
  public class PaymentProfile
  {
    public String PaymentProfileId { get; set; }
    public String CardCode { get; set; }
  }
}
