using System;

namespace RedTail.Payments.AuthorizeNet.CustomerProfileTransaction
{
  public class CustomerProfileTransactionRequest
    : AuthorizeNetRequest
  {
    public MerchantAuthentication MerchantAuthentication => Properties.MerchantAuthentication;
    public String RefId { get; set; }
    public TransactionRequest TransactionRequest { get; set; }
  }
}
