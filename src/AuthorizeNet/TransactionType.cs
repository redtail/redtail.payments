namespace RedTail.Payments.AuthorizeNet
{
  public enum TransactionType
  {
    [EnumModel(Name = "authCaptureTransaction")]
    Charge = 1,

    [EnumModel(Name = "authOnlyTransaction")]
    Authorize = 2,

    [EnumModel(Name = "priorAuthCaptureTransaction")]
    Capture = 3
  }
}
