using System;

namespace RedTail.Payments.AuthorizeNet.DeleteCustomerPaymentProfile
{
  public class DeleteCustomerPaymentProfileRequest
    : AuthorizeNetRequest
  {
    public MerchantAuthentication MerchantAuthentication => Properties.MerchantAuthentication;
    public String CustomerProfileId { get; set; }
    public String CustomerPaymentProfileId { get; set; }
  }
}
