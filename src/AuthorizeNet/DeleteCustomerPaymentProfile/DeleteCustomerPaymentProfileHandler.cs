using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet.DeleteCustomerPaymentProfile
{
  public class DeleteCustomerPaymentProfileHandler
    : AuthorizeNetHandler<DeleteCustomerPaymentProfileRequest, DeleteCustomerPaymentProfileResponse>
  {
    public DeleteCustomerPaymentProfileHandler(DeleteCustomerPaymentProfileRequest request, Boolean stopOnError)
      : base(request, stopOnError)
    {

    }

    private String Json = null;

    protected override void Command(DeleteCustomerPaymentProfileRequest request)
    {
      this.Json = @"
        {
          ""deleteCustomerPaymentProfileRequest"": " + JsonConvert.SerializeObject(request, Formatting.None, Properties.JsonSerializerSettings) + @"
        }
      ";

      this.Send(this.Json);
    }

    protected override void Done(DeleteCustomerPaymentProfileResponse response)
    {
      base.Done(response);
    }

    protected override void Error(DeleteCustomerPaymentProfileResponse response)
    {
      base.Error(response);
    }
  }
}
