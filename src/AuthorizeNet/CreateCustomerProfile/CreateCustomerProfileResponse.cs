using System;
using System.Collections.Generic;

namespace RedTail.Payments.AuthorizeNet.CreateCustomerProfile
{
  public class CreateCustomerProfileResponse
    : AuthorizeNetResponse
  {
    public List<String> CustomerPaymentProfileIdList { get; set; }
    public List<String> CustomerShippingAddressIdList { get; set; }
    public String CustomerProfileId { get; set; }
    public List<String> ValidationDirectResponseList { get; set; }
  }
}
