using System;

namespace RedTail.Payments.AuthorizeNet.CreateCustomerProfile
{
  public class CreateCustomerProfileRequest
    : AuthorizeNetRequest
  {
    public MerchantAuthentication MerchantAuthentication => Properties.MerchantAuthentication;
    public Profile Profile { get; set; }
    public Boolean? ValidationMode
    {
      get
      {
        if (this.Profile.PaymentProfiles == null)
          return null;

        return this.Profile.PaymentProfiles.Count > 0;
      }
    }
  }
}
