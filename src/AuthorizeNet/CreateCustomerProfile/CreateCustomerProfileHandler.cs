using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet.CreateCustomerProfile
{
  public class CreateCustomerProfileHandler
    : AuthorizeNetHandler<CreateCustomerProfileRequest, CreateCustomerProfileResponse>
  {
    public CreateCustomerProfileHandler(CreateCustomerProfileRequest request, Boolean stopOnError)
      : base(request, stopOnError)
    {

    }

    private List<String> CustomerPaymentProfileIdList = null;
    private List<String> CustomerShippingAddressIdList = null;
    private String CustomerProfileId = null;
    private String Json = null;
    private List<String> ValidationDirectResponseList = null;

    protected override void Command(CreateCustomerProfileRequest request)
    {
      this.Json = @"
        {
          ""createCustomerProfileRequest"": " + JsonConvert.SerializeObject(request, Formatting.None, Properties.JsonSerializerSettings) + @"
        }
      ";

      String result = this.Send(this.Json);
      CreateCustomerProfileResponse response = JsonConvert.DeserializeObject<CreateCustomerProfileResponse>(result);

      this.CustomerPaymentProfileIdList = response.CustomerPaymentProfileIdList;
      this.CustomerShippingAddressIdList = response.CustomerShippingAddressIdList;
      this.CustomerProfileId = response.CustomerProfileId;
      this.ValidationDirectResponseList = response.ValidationDirectResponseList;
    }

    protected override void Done(CreateCustomerProfileResponse response)
    {
      base.Done(response);
      response.CustomerPaymentProfileIdList = this.CustomerPaymentProfileIdList;
      response.CustomerShippingAddressIdList = this.CustomerShippingAddressIdList;
      response.CustomerProfileId = this.CustomerProfileId;
      response.ValidationDirectResponseList = this.ValidationDirectResponseList;
    }

    protected override void Error(CreateCustomerProfileResponse response)
    {
      base.Error(response);
    }
  }
}
