using System;

namespace RedTail.Payments.AuthorizeNet
{
  public class MerchantAuthentication
  {
    public String Name { get; set; }
    public String TransactionKey { get; set; }
  }
}
