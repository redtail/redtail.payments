using System;

namespace RedTail.Payments.AuthorizeNet.CreateCustomerPaymentProfile
{
  public class CreateCustomerPaymentProfileResponse
    : AuthorizeNetResponse
  {
    public String CustomerProfileId { get; set; }
    public String CustomerPaymentProfileId { get; set; }
    public String ValidationDirectResponse { get; set; }
  }
}
