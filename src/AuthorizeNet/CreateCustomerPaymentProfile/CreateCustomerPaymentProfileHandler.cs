using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet.CreateCustomerPaymentProfile
{
  public class CreateCustomerPaymentProfileHandler
    : AuthorizeNetHandler<CreateCustomerPaymentProfileRequest, CreateCustomerPaymentProfileResponse>
  {
    public CreateCustomerPaymentProfileHandler(CreateCustomerPaymentProfileRequest request, Boolean stopOnError)
      : base(request, stopOnError)
    {

    }

    private String CustomerProfileId = null;
    private String CustomerPaymentProfileId = null;
    private String Json = null;
    private String ValidationDirectResponse = null;

    protected override void Command(CreateCustomerPaymentProfileRequest request)
    {
      this.Json = @"
        {
          ""createCustomerPaymentProfileRequest"": " + JsonConvert.SerializeObject(request, Formatting.None, Properties.JsonSerializerSettings) + @"
        }
      ";

      String result = this.Send(this.Json);
      CreateCustomerPaymentProfileResponse response = JsonConvert.DeserializeObject<CreateCustomerPaymentProfileResponse>(result);

      this.CustomerProfileId = response.CustomerProfileId;
      this.CustomerPaymentProfileId = response.CustomerPaymentProfileId;
      this.ValidationDirectResponse = response.ValidationDirectResponse;
    }

    protected override void Done(CreateCustomerPaymentProfileResponse response)
    {
      base.Done(response);
      response.CustomerProfileId = this.CustomerProfileId;
      response.CustomerPaymentProfileId = this.CustomerPaymentProfileId;
      response.ValidationDirectResponse = this.ValidationDirectResponse;
    }

    protected override void Error(CreateCustomerPaymentProfileResponse response)
    {
      base.Error(response);
    }
  }
}
