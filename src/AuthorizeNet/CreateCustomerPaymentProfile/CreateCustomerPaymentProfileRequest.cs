using System;

namespace RedTail.Payments.AuthorizeNet.CreateCustomerPaymentProfile
{
  public class CreateCustomerPaymentProfileRequest
    : AuthorizeNetRequest
  {
    public MerchantAuthentication MerchantAuthentication => Properties.MerchantAuthentication;
    public String CustomerProfileId { get; set; }
    public PaymentProfile PaymentProfile { get; set; }
    public String ValidationMode { get; set; }
  }
}
