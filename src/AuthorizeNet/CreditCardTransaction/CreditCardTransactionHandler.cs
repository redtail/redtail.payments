using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet.CreditCardTransaction
{
  public class CreditCardTransactionHandler
    : AuthorizeNetHandler<CreditCardTransactionRequest, CreditCardTransactionResponse>
  {
    public CreditCardTransactionHandler(CreditCardTransactionRequest request, Boolean stopOnError)
      : base(request, stopOnError)
    {

    }

    private TransactionResponse TransactionResponse = null;
    private String Json = null;
    private String RefId = null;

    protected override void Command(CreditCardTransactionRequest request)
    {
      this.Json = @"
        {
          ""createTransactionRequest"": " + JsonConvert.SerializeObject(request, Formatting.None, Properties.JsonSerializerSettings) + @"
        }
      ";

      String result = this.Send(this.Json);
      CreditCardTransactionResponse response = JsonConvert.DeserializeObject<CreditCardTransactionResponse>(result);

      this.TransactionResponse = response.TransactionResponse;
      this.RefId = response.RefId;
    }

    protected override void Done(CreditCardTransactionResponse response)
    {
      base.Done(response);
      response.TransactionResponse = this.TransactionResponse;
      response.RefId = this.RefId;
    }

    protected override void Error(CreditCardTransactionResponse response)
    {
      base.Error(response);
    }
  }
}
