using System;

namespace RedTail.Payments.AuthorizeNet.CreditCardTransaction
{
  public class CreditCardTransactionRequest
    : AuthorizeNetRequest
  {
    public MerchantAuthentication MerchantAuthentication => Properties.MerchantAuthentication;
    public String RefId { get; set; }
    public TransactionRequest TransactionRequest { get; set; }
  }
}
