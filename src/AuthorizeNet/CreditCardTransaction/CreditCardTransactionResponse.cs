using System;

namespace RedTail.Payments.AuthorizeNet.CreditCardTransaction
{
  public class CreditCardTransactionResponse
    : AuthorizeNetResponse
  {
    public TransactionResponse TransactionResponse { get; set; }
    public String RefId { get; set; }
  }
}
