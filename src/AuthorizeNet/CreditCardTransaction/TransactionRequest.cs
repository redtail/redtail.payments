using System;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet.CreditCardTransaction
{
  public class TransactionRequest
  {
    public String TransactionType => this.Type.GetName();
    public String Amount { get; set; }
    public Payment Payment { get; set; }

    [JsonIgnore]
    public TransactionType Type { get; set; }
  }
}
