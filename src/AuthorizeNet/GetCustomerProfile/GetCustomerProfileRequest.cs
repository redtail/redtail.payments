using System;

namespace RedTail.Payments.AuthorizeNet.GetCustomerProfile
{
  public class GetCustomerProfileRequest
    : AuthorizeNetRequest
  {
    public MerchantAuthentication MerchantAuthentication => Properties.MerchantAuthentication;
    public String CustomerProfileId { get; set; }
  }
}
