using System;
using System.Collections.Generic;

namespace RedTail.Payments.AuthorizeNet.GetCustomerProfile
{
  public class GetCustomerProfileResponse
    : AuthorizeNetResponse
  {
    public Profile Profile { get; set; }
    public List<String> SubscriptionIds { get; set; }
  }
}
