using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet.GetCustomerProfile
{
  public class GetCustomerProfileHandler
    : AuthorizeNetHandler<GetCustomerProfileRequest, GetCustomerProfileResponse>
  {
    public GetCustomerProfileHandler(GetCustomerProfileRequest request, Boolean stopOnError)
      : base(request, stopOnError)
    {

    }

    private String Json = null;
    private Profile Profile = null;
    private List<String> SubscriptionIds = null;

    protected override void Command(GetCustomerProfileRequest request)
    {
      this.Json = @"
        {
          ""getCustomerProfileRequest"": " + JsonConvert.SerializeObject(request, Formatting.None, Properties.JsonSerializerSettings) + @"
        }
      ";

      String result = this.Send(this.Json);
      GetCustomerProfileResponse response = JsonConvert.DeserializeObject<GetCustomerProfileResponse>(result);

      this.Profile = response.Profile;
      this.SubscriptionIds = response.SubscriptionIds;
    }

    protected override void Done(GetCustomerProfileResponse response)
    {
      base.Done(response);
      response.Profile = this.Profile;
      response.SubscriptionIds = this.SubscriptionIds;
    }

    protected override void Error(GetCustomerProfileResponse response)
    {
      base.Error(response);
    }
  }
}
