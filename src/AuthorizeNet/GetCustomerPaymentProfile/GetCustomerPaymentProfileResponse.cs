using System;

namespace RedTail.Payments.AuthorizeNet.GetCustomerPaymentProfile
{
  public class GetCustomerPaymentProfileResponse
    : AuthorizeNetResponse
  {
    public PaymentProfile PaymentProfile { get; set; }
  }
}
