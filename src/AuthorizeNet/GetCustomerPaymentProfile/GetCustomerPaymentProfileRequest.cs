using System;

namespace RedTail.Payments.AuthorizeNet.GetCustomerPaymentProfile
{
  public class GetCustomerPaymentProfileRequest
    : AuthorizeNetRequest
  {
    public MerchantAuthentication MerchantAuthentication => Properties.MerchantAuthentication;
    public String CustomerProfileId { get; set; }
    public String CustomerPaymentProfileId { get; set; }
  }
}
