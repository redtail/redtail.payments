using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet.GetCustomerPaymentProfile
{
  public class GetCustomerPaymentProfileHandler
    : AuthorizeNetHandler<GetCustomerPaymentProfileRequest, GetCustomerPaymentProfileResponse>
  {
    public GetCustomerPaymentProfileHandler(GetCustomerPaymentProfileRequest request, Boolean stopOnError)
      : base(request, stopOnError)
    {

    }

    private String Json = null;
    private PaymentProfile PaymentProfile = null;

    protected override void Command(GetCustomerPaymentProfileRequest request)
    {
      this.Json = @"
        {
          ""getCustomerPaymentProfileRequest"": " + JsonConvert.SerializeObject(request, Formatting.None, Properties.JsonSerializerSettings) + @"
        }
      ";

      String result = this.Send(this.Json);
      GetCustomerPaymentProfileResponse response = JsonConvert.DeserializeObject<GetCustomerPaymentProfileResponse>(result);

      this.PaymentProfile = response.PaymentProfile;
    }

    protected override void Done(GetCustomerPaymentProfileResponse response)
    {
      base.Done(response);
      response.PaymentProfile = this.PaymentProfile;
    }

    protected override void Error(GetCustomerPaymentProfileResponse response)
    {
      base.Error(response);
    }
  }
}
