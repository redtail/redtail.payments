using System;
using System.IO;
using System.Net;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet
{
  public abstract class AuthorizeNetHandler<T1, T2>
    : BaseHandler<T1, T2>
    where T1 : AuthorizeNetRequest
    where T2 : AuthorizeNetResponse
  {
    public AuthorizeNetHandler(T1 request, Boolean stopOnError)
      : base(request, stopOnError)
    {

    }

    private Messages Messages = null;
    private String Result = null;

    protected override abstract void Command(T1 request);

    protected override void Done(T2 response)
    {
      response.Messages = this.Messages;
    }

    protected override void Error(T2 response)
    {
      response.Messages = this.Messages;
    }

    protected String Send(String json)
    {
      String url = Properties.TestMode ? Properties.SandboxApiEndpoint : Properties.ProductionApiEndpoint;

      HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
      httpWebRequest.ContentType = "application/json";
      httpWebRequest.Method = "POST";

      using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
      {
        streamWriter.Write(json);
        streamWriter.Flush();
        streamWriter.Close();
      }

      HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
      using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
      {
          this.Result = streamReader.ReadToEnd();

          T2 response = JsonConvert.DeserializeObject<T2>(this.Result);
          this.Messages = response.Messages;

          return this.Result;
      }
    }
  }
}
