using System;

namespace RedTail.Payments.AuthorizeNet
{
  public class CodeToken
  {
    public String Code { get; set; }
    public String Description { get; set; }
  }
}
