using System;

namespace RedTail.Payments.AuthorizeNet
{
  public class CreditCard
  {
    public String CardNumber { get; set; }
    public String ExpirationDate { get; set; }
    public String CardCode { get; set; }
    public String CardType { get; set; }
    public String IssuerNumber { get; set; }
    public Boolean IsPaymentToken { get; set; }
  }
}
