using System;

namespace RedTail.Payments.AuthorizeNet.VoidTransaction
{
  public class VoidTransactionRequest
    : AuthorizeNetRequest
  {
    public MerchantAuthentication MerchantAuthentication => Properties.MerchantAuthentication;
    public String RefId { get; set; }
    public TransactionRequest TransactionRequest { get; set; }
  }
}
