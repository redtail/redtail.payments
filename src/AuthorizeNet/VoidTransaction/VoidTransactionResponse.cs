using System;

namespace RedTail.Payments.AuthorizeNet.VoidTransaction
{
  public class VoidTransactionResponse
    : AuthorizeNetResponse
  {
    public TransactionResponse TransactionResponse { get; set; }
    public String RefId { get; set; }
  }
}
