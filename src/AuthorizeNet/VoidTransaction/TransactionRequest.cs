using System;

namespace RedTail.Payments.AuthorizeNet.VoidTransaction
{
  public class TransactionRequest
  {
    public String TransactionType { get; } = "voidTransaction";
    public String RefTransId { get; set; }
  }
}
