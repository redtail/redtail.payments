using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

using Newtonsoft.Json;

namespace RedTail.Payments.AuthorizeNet.VoidTransaction
{
  public class VoidTransactionHandler
    : AuthorizeNetHandler<VoidTransactionRequest, VoidTransactionResponse>
  {
    public VoidTransactionHandler(VoidTransactionRequest request, Boolean stopOnError)
      : base(request, stopOnError)
    {

    }

    private TransactionResponse TransactionResponse = null;
    private String Json = null;
    private String RefId = null;

    protected override void Command(VoidTransactionRequest request)
    {
      this.Json = @"
        {
          ""createTransactionRequest"": " + JsonConvert.SerializeObject(request, Formatting.None, Properties.JsonSerializerSettings) + @"
        }
      ";

      String result = this.Send(this.Json);
      VoidTransactionResponse response = JsonConvert.DeserializeObject<VoidTransactionResponse>(result);

      this.TransactionResponse = response.TransactionResponse;
      this.RefId = response.RefId;
    }

    protected override void Done(VoidTransactionResponse response)
    {
      base.Done(response);
      response.TransactionResponse = this.TransactionResponse;
      response.RefId = this.RefId;
    }

    protected override void Error(VoidTransactionResponse response)
    {
      base.Error(response);
    }
  }
}
