using System;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace RedTail.Payments.AuthorizeNet
{
  public static class Properties
  {
    public static MerchantAuthentication MerchantAuthentication { get; set; }
    public static Boolean TestMode { get; set; }

    public static String ProductionApiEndpoint = "https://api.authorize.net/xml/v1/request.api";
    public static String SandboxApiEndpoint = "https://apitest.authorize.net/xml/v1/request.api";

    public static JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
    {
      ContractResolver = new CamelCasePropertyNamesContractResolver(),
      NullValueHandling = NullValueHandling.Ignore
    };
  }
}
