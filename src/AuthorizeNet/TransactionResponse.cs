using System;
using System.Collections.Generic;

namespace RedTail.Payments.AuthorizeNet
{
  public class TransactionResponse
  {
    public String ResponseCode { get; set; }
    public String AuthCode { get; set; }
    public String AvsResultCode { get; set; }
    public String CvvResultCode { get; set; }
    public String CavvResultCode { get; set; }
    public String TransId { get; set; }
    public String RefTransID { get; set; }
    public String TransHash { get; set; }
    public String AccountNumber { get; set; }
    public String AccountType { get; set; }
    public List<Error> Errors { get; set; }
    public List<CodeToken> Messages { get; set; }
  }
}
