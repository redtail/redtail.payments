namespace RedTail.Payments.AuthorizeNet
{
  public class AuthorizeNetResponse
    : BaseResponse
  {
    public Messages Messages { get; set; }
  }
}
