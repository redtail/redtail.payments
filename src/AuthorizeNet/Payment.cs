using System;

namespace RedTail.Payments.AuthorizeNet
{
  public class Payment
  {
    public CreditCard CreditCard { get; set; }
  }
}
